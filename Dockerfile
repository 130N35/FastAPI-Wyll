FROM tiangolo/uvicorn-gunicorn:python3.9-alpine3.14

WORKDIR /api
COPY requirements.txt .

RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY . .

EXPOSE 8000 27017
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
