# FastAPI-Wyll

A simple API developped using FastAPI and MongoDB.

## Features
* Product management
	* GET /products/  
		`Get all products`
	* GET /products/{product_id}  
		`Get one product by ID`
	* POST /products/  
		`Create a product`
	* PUT /products/{product_id}  
		`Update a product`
	* DELETE /products/{product_id}  
		`Delete a product by ID`

## Requirements
Tested with Python 3.9  
  
* fastapi==0.68.0
* uvicorn==0.12.2
* pydantic[dotenv]
* requests
* pymongo
* mongoengine
* pytest
* flake8

## Clone the project
```console
$ git clone https://gitlab.com/130N35/FastAPI-Wyll
```

## Run in local
### Install dependencies
```console
$ pip install -r requirements.txt
```

### Run the server
```console
$ uvicorn app.main:app --reload
```

## Deploy with Docker

Use `docker-compose` to build the docker images and run the containers
```console
$ docker-compose up -d --build
```

## Access the application

You can now access the API at the address `http://127.0.0.1:8000/`

### Execute tests
Simply run `pytest` at the base of the project to run all tests
```console
$ pytest
```

## API documentation
You can either access to the documentation provided by Swagger UI at `http://127.0.0.1:8000/docs`  
or the one provided by Redoc at `http://127.0.0.1:8000/redoc`
