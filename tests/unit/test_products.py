import pytest


def test_get_all_products(test_client):
    # test with no product found
    response = test_client.get("/products/")
    assert response.status_code == 200
    assert response.json() == []

    # test with multiple products found
    product_id = []
    for i in range(2):
        data = {
            "name": f"poster_{i}", "price": 10.0 * i,
            "description": "a beautiful poster"
        }

        response = test_client.post("/products/", json=data)
        product_id.append(["product_id"])

    response = test_client.get("/products/")
    assert response.status_code == 200
    assert len(response.json()) == 2

    for i in range(2):
        assert response.json()[i]["name"] == f"poster_{i}"
        assert response.json()[i]["price"] == 10.0 * i


def test_create_product(test_client):
    data = {
        "name": "poster", "price": 10.0,
        "description": "a beautiful poster"
    }
    response = test_client.post("/products/", json=data)
    assert response.status_code == 201
    assert response.json()["message"] == "Product created successfully"
    assert "product_id" in response.json()


def test_get_one_product(test_client):
    data = {
        "name": "poster", "price": 10.0,
        "description": "a beautiful poster"
    }
    response = test_client.post("/products/", json=data)
    product_id = response.json()["product_id"]

    response = test_client.get(f"/products/{product_id}")
    assert response.status_code == 200
    assert response.json()["name"] == "poster"
    assert response.json()["price"] == 10.0


def test_update_product(test_client):
    data = {
        "name": "poster", "price": 10.0,
        "description": "a beautiful poster"
    }
    response = test_client.post("/products/", json=data)
    product_id = response.json()["product_id"]

    data = {
        "name": "updated poster", "price": 12.0,
        "description": "a beautiful and updated poster"
    }
    response = test_client.put(f"/products/{product_id}", json=data)
    assert response.status_code == 200
    assert response.json()["message"] == "Product updated successfully"

    response = test_client.get(f"/products/{product_id}")
    assert response.status_code == 200
    assert response.json()["name"] == "updated poster"
    assert response.json()["price"] == 12.0
    assert response.json()["description"] == "a beautiful and updated poster"


def test_delete_product(test_client):
    data = {
        "name": "poster", "price": 10.0,
        "description": "a beautiful poster"
    }
    response = test_client.post("/products/", json=data)
    product_id = response.json()["product_id"]

    response = test_client.delete(f"/products/{product_id}")
    assert response.status_code == 200
    assert response.json()["message"] == "Product deleted successfully"

    response = test_client.get(f"/products/{product_id}")
    assert response.status_code == 404


if __name__ == "__main__":
    pytest.main()
