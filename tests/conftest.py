import pytest
from app.database import connect_to_database, disconnect_from_database, drop_test_database
from app.main import app
from fastapi.testclient import TestClient


@pytest.fixture(scope="session")
def test_client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session")
def setup_test_database():
    # setup
    connect_to_database(test=True)
    yield

    # teardown
    disconnect_from_database()
    drop_test_database()


def pytest_sessionfinish(session, exitstatus):
    drop_test_database()
