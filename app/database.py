from mongoengine import connect, disconnect

from .settings import mongo_settings as settings


def connect_to_database(test: bool = False):
    """
        Connect to MongoDB database, default or test

        :param test: Boolean to tell if the application is in test mode
        :type test: bool
    """
    if test:
        database = settings.db
    else:
        database = f"TEST_{settings.db}"

    connect(
        database,
        host=settings.host,
        port=settings.port,
        alias="test" if test else "default"
    )


def disconnect_from_database():
    disconnect()


def drop_test_database():
    """
        Drop all the test database after
    """
    disconnect()
    connection = connect(f"TEST_{settings.db}")
    connection.drop_database(f"TEST_{settings.db}")
    disconnect()
