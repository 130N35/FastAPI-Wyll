from mongoengine import Document, StringField, FloatField


class MongoProduct(Document):
    name: str = StringField(required=True, max_length=100)
    price: float = FloatField(required=True, min_value=0)
    description: str = StringField(max_length=500)

    meta = {'collection': 'products'}
