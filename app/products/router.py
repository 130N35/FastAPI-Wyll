from fastapi import APIRouter, HTTPException, status
from app.products.mongo_models import MongoProduct
from app.products.pydantic_models import (
    Product, Message404, MessageUpdate200,
    MessageDelete200, MessageCreate201
)

import json

router = APIRouter()


@router.get(
    "/",
    summary="Get all products",
    response_model=list[Product]
)
def get_all_products():
    products = MongoProduct.objects().to_json()
    return json.loads(products)


@router.post(
    "/",
    summary="Create a product",
    status_code=status.HTTP_201_CREATED,
    response_model=MessageCreate201
)
def create_product(product: Product):
    m_product = MongoProduct(**product.dict()).save()
    return {
        "message": "Product created successfully",
        "product_id": str(m_product.id)
    }


@router.get(
    "/{product_id}",
    summary="Get a product by ID",
    response_model=Product,
    responses={404: {"model": Message404}},
)
def get_product(product_id: str):
    try:
        product = MongoProduct.objects.get(id=product_id)
        return Product(**product.to_mongo())
    except MongoProduct.DoesNotExist:
        raise HTTPException(status_code=404, detail="Product not found")


@router.put(
    "/{product_id}",
    summary="Update a product by ID",
    response_model=MessageUpdate200,
    responses={404: {"model": Message404}},
)
def update_product(product_id: str, product: Product):
    try:
        existing_product = MongoProduct.objects.get(id=product_id)
        existing_product.update(**product.dict())
        return {"message": "Product updated successfully"}
    except MongoProduct.DoesNotExist:
        raise HTTPException(status_code=404, detail="Product not found")


@router.delete(
    "/{product_id}",
    summary="Delete a product by ID",
    response_model=MessageDelete200,
    responses={404: {"model": Message404}},
)
def delete_product(product_id: str):
    try:
        product = MongoProduct.objects.get(id=product_id)
        product.delete()
        return {"message": "Product deleted successfully"}
    except MongoProduct.DoesNotExist:
        raise HTTPException(status_code=404, detail="Product not found")
