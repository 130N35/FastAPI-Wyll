from typing import Union
from pydantic import BaseModel, Field


class Product(BaseModel):
    name: str = Field(example="poster")
    price: float = Field(example=12.5)
    description: Union[str, None] = Field(
        default=None, example="a beautiful poster"
    )


class Message(BaseModel):
    """
        Basic message model
    """
    message: str


class Message404(BaseModel):
    """
        Message model with error 404 generic example
    """
    message: str = Field(example="Product not found")


class MessageUpdate200(BaseModel):
    """
        Message model with successful response 200 generic
        example for UPDATE routes
    """
    message: str = Field(example="Product updated successfully")


class MessageDelete200(BaseModel):
    """
        Message model with successful response 200 generic
        example for DELETE routes
    """
    message: str = Field(example="Product deleted successfully")


class MessageCreate201(BaseModel):
    """
        Message model with successful response 201 generic
        example for CREATE routes
    """
    message: str = Field(example="Product not found")
    product_id: str = Field(example="647be5655b4b09c22dacc12e")
