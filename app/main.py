from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi

from app.products.router import router as product_router
from app.products.pydantic_models import Message
from app.database import connect_to_database
from app.settings import api_settings as settings


def custom_openapi():
    """
        Define a custom OpenAPI schema for the application

        :return: OpenAPI schema
    """
    tags_metadata = [
        {
            "name": "Healthchecker",
            "description": "Check live information about the API"
        },
        {
            "name": "Products", "description": "Manage products"
        }
    ]

    if app.openapi_schema:
        return app.openapi_schema

    openapi_schema = get_openapi(
        title=settings.name,
        version=settings.project_version,
        description="This API helps you manage your products.",
        terms_of_service="http://example.com/terms/",
        contact={
            "name": "Kevin Penin",
            "url": "http://kevinpenin.fr/",
            "email": "contact@kevinpenin.fr",
        },
        license_info={
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
        routes=app.routes,
    )
    openapi_schema["tags"] = tags_metadata
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


def get_application():
    """
        Return application with needed middlewares

        :return: FastAPI application
    """
    _app = FastAPI()

    origins = [str(origin) for origin in settings.backend_cors_origins]

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return _app


connect_to_database()

app = get_application()
app.openapi = custom_openapi
app.include_router(product_router, prefix="/products", tags=["Products"])


@app.get(
    "/",
    summary="Check if API is alive",
    tags=["Healthchecker"],
    response_model=Message,
    responses={
        200: {
            "content": {
                "application/json": {
                    "example": {"message": "API is working."}
                }
            },
        },
    }
)
def root():
    return {"message": "API is working."}
