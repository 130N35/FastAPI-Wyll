from pydantic import AnyHttpUrl, BaseSettings, validator
from typing import List, Union

__all__ = ("api_settings", "mongo_settings")


class BaseSettings(BaseSettings):
    class Config:
        env_file = ".env"


class APISettings(BaseSettings):
    project_version: str = "0.0.1"
    backend_cors_origins: List[AnyHttpUrl] = []
    log_level: str = "INFO"
    name: str
    host: str
    port: int

    @validator("backend_cors_origins", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    class Config(BaseSettings.Config):
        env_prefix = "API_"


class MongoSettings(BaseSettings):
    host: str
    port: int
    db: str

    class Config(BaseSettings.Config):
        env_prefix = "MONGO_"


api_settings = APISettings()
mongo_settings = MongoSettings()
